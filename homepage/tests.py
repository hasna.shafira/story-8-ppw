from django.test import TestCase, Client
from .views import index
from django.urls import resolve
from django.http import HttpRequest
from django.apps import apps

# Create your tests here.
class TestStory8(TestCase):
    def test_url_story8(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)
    def test_template_story8(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
    def test_text_story8_dihalaman(self):
        response = Client().get('/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Story 8", html_kembalian)
    def test_url_json(self):
        response = Client().get("/data/")
        self.assertEqual(response.status_code, 200)
